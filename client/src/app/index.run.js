(function() {
  'use strict';

  angular
    .module('source')
    .run(runBlock);
    
  runBlock.$inject = ['$rootScope','$log'];  
  function runBlock($rootScope, $log) {

    $rootScope.$on('$destroy', onDestroy);
    var stateChangeStart = $rootScope.$on('$stateChangeStart', stateChange);
    $log.debug('runBlock end');

    function stateChange(event, toState) {
      $rootScope.title = toState.views.bodyView.data.title;
    }

    function onDestroy() {
      stateChangeStart();
    }
  }

})();
