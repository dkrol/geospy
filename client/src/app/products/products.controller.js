(function() {
'use strict';

  angular
    .module('ProductsModule', [])
    .controller('ProductsController', ProductsController);

  ProductsController.$inject = ['ProductsService'];
  function ProductsController(ProductsService) {
    var vm = this;
    vm.products = null;

    initProducts();

    function initProducts() {
      ProductsService.getProducts()
        .then(getProductsSuccess)
        .catch(getProductsFailed);

      function getProductsSuccess(response) {
        
        vm.products = response;
        console.log(vm.products);

        if (response.data) {
          vm.products = response.data;
          console.log(vm.products);
        }
      }

      function getProductsFailed(error) {
        console.log('ERROR');
        console.log(error);
      }
    }

  }
})();