(function() {
'use strict';

  angular
    .module('source')
    .factory('ProductsService', ProductsService);

  ProductsService.$inject = ['$http','$resource'];
  function ProductsService($http, $resource) {
    var urlIIS = 'http://192.168.0.101:9810/api/products';
    var url = 'http://localhost:28424/api/products';
    var service = {
      getProducts: getProducts
    };
    
    return service;
    
    function getProducts() {
      // return $http({
      //   method: 'GET',
      //   url: url
      // });

      var resource = $resource(url);

      return resource.query().$promise;
    }

  }
})();