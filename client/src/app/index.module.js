(function() {
  'use strict';

  angular
    .module('source', [
      'ngAnimate', 
      'ngCookies', 
      'ngMessages', 
      'ui.router', 
      'ui.bootstrap', 
      'toastr', 
      'ngMaterial', 
      'ngResource',
      'HeaderModule',
      'MainBodyModule',
      'HomeModule',
      'ProductsModule'
      ]);
})();
