(function() {
  'use strict';

  angular
    .module('source')
    .config(routerConfig);


  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        views: {
          'bodyView': {
            templateUrl: 'app/home/home.view.html',
            controller: 'HomeController',
            controllerAs: 'homeCtrl',
            data: {
              title: 'Home'
            }
          }
        }
      })
      .state('products', {
        url: '/',
        views: {
          'bodyView': {
            templateUrl: 'app/products/products.view.html',
            controller: 'ProductsController',
            controllerAs: 'ptrCtrl',
            data: {
              title: 'Products'
            }
          }
        }
      });
      
    $urlRouterProvider.otherwise('/');
  }

})();
