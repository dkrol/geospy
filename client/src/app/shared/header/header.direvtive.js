(function() {
  'use strict';

  angular
    .module('HeaderModule', [])
    .directive('header', header);

  function header() {
    var directive = {
        templateUrl: 'app/shared/header/header.view.html',
        restrict: 'E',
        scope: {
          title: '='
        }
    };
    return directive;
  }
})();