(function() {
  'use strict';

  angular
    .module('MainBodyModule', [])
    .directive('mainBody', mainBody);

  function mainBody() {
    var directive = {
        templateUrl: 'app/main-body/main-body.view.html',
        controller: 'MainBodyController',
        controllerAs: 'mainBodyCtrl',
        restrict: 'E',
        scope: {
        }
    };
    return directive;
  }

})();