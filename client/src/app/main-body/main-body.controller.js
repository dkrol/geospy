(function() {
'use strict';

  angular
    .module('MainBodyModule')
    .controller('MainBodyController', MainBodyController);

  MainBodyController.$inject = ['$mdSidenav','$state'];
  function MainBodyController($mdSidenav, $state) {
    var vm = this;
    vm.toggleMenu = toggleMenu;
    vm.menuHeaderClick = menuHeaderClick;
    vm.menuItemClick = menuItemClick;
    vm.menuItems = [];
    
    initMenuItems();

    function initMenuItems() {
      vm.menuItems.push({
        header: 'Home',
        uisref: 'home',
        iconClass: 'fa fa-home',
        isSelected: true
      });

      vm.menuItems.push({
        header: 'Products',
        uisref: 'products',
        iconClass: 'fa fa-tasks',
        isSelected: false
      });
    }
    
    function toggleMenu() {
      $mdSidenav('left').toggle();
    }

    function menuHeaderClick() {
      cleanSelectedItem();
      vm.menuItems[0].isSelected = true;
      $state.go('home');
    }

    function menuItemClick(index) {
      cleanSelectedItem();
      vm.menuItems[index].isSelected = true;
    }

    function cleanSelectedItem() {
      for (var i = 0, length = vm.menuItems.length; i < length; i++) {
        vm.menuItems[i].isSelected = false;
      }
    }
  }
})();