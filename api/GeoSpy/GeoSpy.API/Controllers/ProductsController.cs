﻿using GeoSpy.API.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace GeoSpy.API.Controllers
{
    public class ProductsController : ApiController
    {
        private ProductRepository productRepository = new ProductRepository();
        // GET: api/Products
        public IEnumerable<Product> Get()
        {
            return productRepository.Retrive();
        }

        // GET: api/Products/5
        public Product Get(int id)
        {
            return productRepository.Retrive(id);
        }

        // POST: api/Products
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/Products/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Products/5
        public void Delete(int id)
        {
        }
    }
}
