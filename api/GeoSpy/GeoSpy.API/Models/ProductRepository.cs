﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web.Hosting;

namespace GeoSpy.API.Models
{
    public class ProductRepository
    {
        private const string PRODUCT_PATH = @"~/App_Data/product.json";
        internal Product Create()
        {
            Product product = new Product
            {
                ReleaseDate = DateTime.Now
            };
            return product;
        }

        internal List<Product> Retrive()
        {
            var filePath = HostingEnvironment.MapPath(PRODUCT_PATH);
            var json = File.ReadAllText(filePath);
            var products = JsonConvert.DeserializeObject<List<Product>>(json);
            return products;
        }

        internal Product Retrive(int id)
        {
            var products = this.Retrive();
            var product = products.Where(p => p.ProductId == id).FirstOrDefault();
            return product;
        }

        internal Product Save(Product product)
        {
            var products = this.Retrive();
            var maxId = products.Max(p => p.ProductId);
            product.ProductId = maxId + 1;
            products.Add(product);
            WriteData(products);
            return product;
        }

        internal Product Save(int id, Product product)
        {
            var products = this.Retrive();
            var itemIndex = products.FindIndex(p => p.ProductId == id);
            if (itemIndex > 0)
            {
                products[itemIndex] = product;
            }
            else
            {
                return null;
            }
            WriteData(products);
            return product;
        }

        private bool WriteData(List<Product> products)
        {
            var filePath = HostingEnvironment.MapPath(PRODUCT_PATH);
            var json = JsonConvert.SerializeObject(products, Formatting.Indented);
            File.WriteAllText(filePath, json);
            return true;
        }
    }
}